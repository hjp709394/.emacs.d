;;;;;;;;;;;;;;;;;;;;;;;;;;;;; package ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'package)
(add-to-list 'package-archives '("melpa2" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)

(setq package-list '(elpy evil autopair company irony company-irony xcscope))
(require 'package)
(add-to-list 'package-archives
						 '("melpa2" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives
						 '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives
						 '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives
						 '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives
						 '("popkit" . "http://elpa.popkit.org/packages/"))
(package-initialize)

; fetch the list of packages available
(unless package-archive-contents
	(package-refresh-contents))
; install the missing packages
(dolist (package package-list)
	(unless (package-installed-p package)
		(package-install package)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; semantic ; (working, uncertain, good);;;;;;;;;;;;;;;;;;;;
;;;; freq-usage:
;;;;    1. C-c ] jump to definition, slowly but accurate
;;;;    2. C-x left_arrow jump back from C-c ]
;;;;
;;;; the build-in cedet is outdated, to use newer version,
;;;;    git clone http://git.code.sf.net/p/cedet/git cedet
;;;; into the ~/.emacs.d/manual-install/ folder 
;;;; and make under cedet and cedet/contrib folder

(add-to-list 'load-path (expand-file-name "~/.emacs.d/manual-install/cedet"))
(add-to-list 'load-path (expand-file-name "~/.emacs.d/manual-install/cedet/contrib"))
(load-file "~/.emacs.d/manual-install/cedet/cedet-devel-load.el")
(load-file "~/.emacs.d/manual-install/cedet/contrib/eassist.el")

(require 'ede)
(global-ede-mode t)
(semantic-load-enable-gaudy-code-helpers)
(semantic-load-enable-excessive-code-helpers)
(require 'cedet)
(require 'cedet-contrib)
(require 'semantic)
(setq semantic-load-turn-useful-things-on t)

(require 'semantic)

;(add-to-list 'semantic-default-submodes 'global-semantic-idle-summary-mode t)
;(add-to-list 'semantic-default-submodes 'global-semantic-idle-completions-mode t)
;(add-to-list 'semantic-default-submodes 'global-cedet-m3-minor-mode t)

(global-ede-mode 1)                      ; Enable the Project management system
(semantic-load-enable-code-helpers)      ; Enable prototype help and smart completion 
(global-srecode-minor-mode 1)            ; Enable template insertion menu
(semantic-mode 1)

(require 'semantic/bovine/gcc)

;;; user-case: C-c ] -> jump to definition => C-x b -> jump back 
;;; (require semantic-mru-bookmark-mode minor mode)
(defun my-cedet-hook ()
  (local-set-key [(control return)] 'semantic-ia-complete-symbol)
  (local-set-key "\C-c?" 'semantic-ia-complete-symbol-menu)
  (local-set-key "\C-c]" 'semantic-ia-fast-jump)
  (local-set-key "\C-c>" 'semantic-complete-analyze-inline)
  (local-set-key "\C-cg" 'semantic-symref)
  (local-set-key "\C-cG" 'semantic-symref-symbol)
  (local-set-key "\C-cp" 'semantic-analyze-proto-impl-toggle))
(add-hook 'c-mode-common-hook 'my-cedet-hook)

;;;; not used currently, these configs are kept for future referance
(defconst cedet-user-include-dirs
  (list "." "./include" "./inc" "./common" "./public"
				".." "../include" "../inc" "../common" "../public"
        "../.." "../../include" "../../inc" "../../common" "../../public"))
(defconst cedet-win32-include-dirs
  (list "C:/MinGW/include"
        "C:/MinGW/include/c++/3.4.5"
        "C:/MinGW/include/c++/3.4.5/mingw32"
        "C:/MinGW/include/c++/3.4.5/backward"
        "C:/MinGW/lib/gcc/mingw32/3.4.5/include"
        "C:/Program Files/Microsoft Visual Studio/VC98/MFC/Include"))
(require 'semantic-c nil 'noerror)
(let ((include-dirs cedet-user-include-dirs))
  (when (eq system-type 'windows-nt)
    (setq include-dirs (append include-dirs cedet-win32-include-dirs)))
  (mapc (lambda (dir)
          (semantic-add-system-include dir 'c++-mode)
          (semantic-add-system-include dir 'c-mode))
        include-dirs))

(defun my-c-mode-common-hook ()
  (define-key c-mode-base-map (kbd "M-o") 'eassist-switch-h-cpp)
  (define-key c-mode-base-map (kbd "M-m") 'eassist-list-methods))
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; elpy (for ref) ;;;;;;;;;;;;;;;;;;;;;;;;;
;;; removed: completion is not as powerful as jedi; not completing properly for multi-returned values
;;; pre-requirements: pip install jedi ropemode
;;; tutorial: http://elpy.readthedocs.org/en/latest/ide.html
;;; freq-usage: 
;;;       1. M-. (elpy-goto-definition)
;;;       2. M-* (pop-tag-mark)
;;;       3. M-Tab (elpy-company-backend)
;;;       4. C-c C-d check document
(require 'elpy nil t)  
(elpy-enable)
(setq elpy-rpc-backend "jedi")
;;; by default elpy uses M-<tab> which is used by os to switch windows
;;; changed it to C-tab for completion
(add-hook 'python-mode-hook
	  '(lambda ()
	     (local-set-key (kbd "C-<tab>") 'elpy-company-backend)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;; evil ; (working, good) ;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; freq-usage:
;;;     1. C-z switch evil on and off
(require 'evil)
(evil-mode 1)
;;; in eshell, i need emacs mode
(evil-set-initial-state 'eshell-mode 'emacs)

;;;; map evil-insert-state into emacs-state
;;;; this line would disable the colum mode of vim
;(defalias 'evil-insert-state 'evil-emacs-state)
;;; use escape to switch to evil-normal-state while in emacs-state
(define-key evil-emacs-state-map [escape] 'evil-normal-state)
;; remove all keybindings from insert-state keymap
(setcdr evil-insert-state-map nil)
;; but [escape] should switch back to normal state
(define-key evil-insert-state-map [escape] 'evil-normal-state)
;; * # search symbol instead of word
(setq-default evil-symbol-word-search t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ein (good) ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; site: https://github.com/millejoh/emacs-ipython-notebook
;;; freq-usages:
;;;    1. start server: in shell -> jupyter notebook
;;;    2. M-x ein:notebooklist-open   | to open the notebook server
;;;    3. M-. jump to definition; M-, jump back
(require 'ein)

;; Or, to enable "superpack" (a little bit hacky improvements):
;; (setq ein:use-auto-complete-superpack t)
(setq ein:use-auto-complete t)

(setq ein:use-smartrep t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; company jedi (good) ;;;;;;;;;;;;;;;;
;(defun my/python-mode-hook ()
;  (add-to-list 'company-backends 'company-jedi))
;(add-hook 'python-mode-hook 'my/python-mode-hook)
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; jedi (good) ;;;;;;;;;;;;;;;;;;;;;;;;
;; don't use default keybindings from jedi.el; keep C-. free
;(setq jedi:setup-keys nil)
;(setq jedi:tooltip-method nil)
;(autoload 'jedi:setup "jedi" nil t)
;(add-hook 'python-mode-hook 'jedi:setup)
;(setq jedi:complete-on-dot t)
;
;(defvar jedi:goto-stack '())
;(defun jedi:jump-to-definition ()
;  (interactive)
;  (add-to-list 'jedi:goto-stack
;               (list (buffer-name) (point)))
;  (jedi:goto-definition))
;(defun jedi:jump-back ()
;  (interactive)
;  (let ((p (pop jedi:goto-stack)))
;    (if p (progn
;            (switch-to-buffer (nth 0 p))
;            (goto-char (nth 1 p))))))
;
;; redefine jedi's C-. (jedi:goto-definition)
;; to remember position, and set C-, to jump back
;(add-hook 'python-mode-hook
;	'(lambda ()
;	 (local-set-key (kbd "C-.") 'jedi:jump-to-definition)
;	 (local-set-key (kbd "C-,") 'jedi:jump-back)
;	 (local-set-key (kbd "C-c d") 'jedi:show-doc)
;	 (local-set-key (kbd "C-<tab>") 'jedi:complete)))
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;; python setting ;;;;;;;;;;;;;;;
;;; pre-requirements: ipython
(setq
 python-shell-interpreter "ipython"
 python-shell-interpreter-args "--colors=Linux --profile=default"
 python-shell-prompt-regexp "In \\[[0-9]+\\]: "
 python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
 python-shell-completion-setup-code
 "from IPython.core.completerlib import module_completion"
 python-shell-completion-module-string-code
 "';'.join(module_completion('''%s'''))\n"
 python-shell-completion-string-code
 "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")

;;;;;;;;;;;;;;;;;;;;;;;;;;;; auto-install ;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'auto-install)
(setq auto-install-directory "~/.emacs.d/auto-install/")
;(auto-install-update-emacswiki-package-name t)
(add-to-list 'load-path (expand-file-name "~/.emacs.d/auto-install/"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;; icicles ;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'icicles)

;;;;;;;;;;;;;;;;;;;;;;;;;;;; xcscope ;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'xcscope)
(cscope-setup)
;;;; key bindings
; C-c s s         Find symbol.
; C-c s d         Find global definition.
; C-c s g         Find global definition (alternate binding).
; C-c s G         Find global definition without prompting.
; C-c s c         Find functions calling a function.
; C-c s C         Find called functions (list functions called
;                 from a function).
; C-c s t         Find text string.
; C-c s e         Find egrep pattern.
; C-c s f         Find a file.
; C-c s i         Find files #including a file.
; 
;;;; useful key binding when switching between search result
; 
; C-c s b         Display *cscope* buffer.
; C-c s B         Auto display *cscope* buffer toggle.
; C-c s n         Next symbol.
; C-c s N         Next file.
; C-c s p         Previous symbol.
; C-c s P         Previous file.
; C-c s u         Pop mark.


;;;;;;;;;;;;;;;; auto complete and yasnippet ; (working, good);;;;;;;;;;;;;;
;(require 'auto-complete)
;(require 'auto-complete-exuberant-ctags)
;(ac-exuberant-ctags-setup)
;(add-to-list 'ac-dictionary-directories "~/.emacs.d/elpa/auto-complete-20151211.227/dict")

(require 'yasnippet)
(yas-global-mode 1)
;;; auto complete mod
;;; should be loaded after yasnippet so that they can work together
;(require 'auto-complete-config)
;;(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
;(ac-config-default)
;;; set the trigger key so that it can work together with yasnippet on tab key,
;;; if the word exists in yasnippet, pressing tab will cause yasnippet to
;;; activate, otherwise, auto-complete will
;(ac-set-trigger-key "TAB")
;(ac-set-trigger-key "<tab>")

(require 'cc-mode)
;(require 'auto-complete-clang)
;(define-key c++-mode-map (kbd "C-<return>") 'ac-complete-clang)

;(require 'member-functions)
;(setq mf--source-file-extension "cpp")

;;;;;;;;;;;;;;;;;;;;;;;;;; autopair ;;;;;;;;;;;;;;;
(require 'autopair)
(autopair-global-mode 1)
(setq autopair-autowrap t)

;;;;;;;;;;;;;;;;;;;;;;;; fonts setting for windows ;;;;;;;;;;;;;;;;;
;(defun qiang-font-existsp (font)
;  (if (null (x-list-fonts font))
;    nil t))
;(defvar font-list '("Microsoft Yahei" "文泉驿等宽微米黑" "黑体" "新宋体" "宋体"))
;(require 'cl) ;; find-if is in common list package
;(find-if #'qiang-font-existsp font-list)
;(defun qiang-make-font-string (font-name font-size)
;  (if (and (stringp font-size) 
;           (equal ":" (string (elt font-size 0))))
;    (format "%s%s" font-name font-size)
;    (format "%s %s" font-name font-size)))
;(defun qiang-set-font (english-fonts
;                        english-font-size
;                        chinese-fonts
;                        &optional chinese-font-size)
;  "english-font-size could be set to \":pixelsize=18\" or a integer.
;If set/leave chinese-font-size to nil, it will follow english-font-size"
;(require 'cl)                         ; for find if
;(let ((en-font (qiang-make-font-string
;                 (find-if #'qiang-font-existsp english-fonts)
;                 english-font-size))
;			(zh-font (font-spec :family (find-if #'qiang-font-existsp chinese-fonts)
;													:size chinese-font-size)))
;	;; Set the default English font 
;	;; 
;	;; The following 2 method cannot make the font settig work in new frames.
;	;; (set-default-font "Consolas:pixelsize=18")
;	;; (add-to-list 'default-frame-alist '(font . "Consolas:pixelsize=18"))
;	;; We have to use set-face-attribute
;	(message "Set English Font to %s" en-font)
;	(set-face-attribute
;	 'default nil :font en-font)
;
;	;; Set Chinese font 
;	;; Do not use 'unicode charset, it will cause the english font setting invalid
;	(message "Set Chinese Font to %s" zh-font)
;	(dolist (charset '(kana han symbol cjk-misc bopomofo))
;		(set-fontset-font (frame-parameter nil 'font)
;											charset
;											zh-font))))
;(qiang-set-font
; '("Courier New" "Consolas" "Monaco" "DejaVu Sans Mono" "Monospace") ":pixelsize=12"
; '("Microsoft Yahei" "文泉驿等宽微米黑" "黑体" "新宋体" "宋体"))

;;;;;;;;;;;;;;;;;;;; irony ; (working, good) ;;;;;;;;;;;;;;;;;;;;;;
;;; notice: irony requires a .clang_compelte file in the root dir 
;;; before it starts to work

;(require 'company)
;(company-mode 1)
;
;(add-hook 'c++-mode-hook 'company-mode)
;(add-hook 'c-mode-hook 'company-mode)
;
;;;; irony-mode
;(require 'irony)
;(require 'irony-cdb)
;(add-hook 'c++-mode-hook 'irony-mode)
;(add-hook 'c-mode-hook 'irony-mode)
;(add-hook 'objc-mode-hook 'irony-mode)
;
;(require 'company-irony)
;(eval-after-load 'company
;                 '(progn
;                    (setq company-idle-delay 0)
;										(setq company-minimum-prefix-length 1)
;                    (add-to-list 'company-backends 'company-irony)))
;
;;;; company-dabbrev make it extremely slow
;(add-to-list 'company-backends '(company-capf company-dabbrev))
;
;;; replace the `completion-at-point' and `complete-symbol' bindings in
;;; irony-mode's buffers by irony-mode's function
;(defun my-irony-mode-hook ()
;  (define-key irony-mode-map [remap completion-at-point]
;              'irony-completion-at-point-async)
;  (define-key irony-mode-map [remap complete-symbol]
;              'irony-completion-at-point-async))
;(add-hook 'irony-mode-hook 'my-irony-mode-hook)
;(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
;
;(require 'irony-cdb-json)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; my functions ;;;;;;;;;;;;;;;;;;;;;;;;;
;;; tags create function 
(defun create-tags (dir-name)
	"Create tags file."
	(interactive "DDirectory: ")
	(shell-command 
	 (format "find %s -type f -regex \".*\\.\\(\\(cc\\)\\|\\(h\\)\\|\\(c\\)\\|\\(cpp\\)\\|\\(cxx\\)\\|\\(hpp\\)\\|\\(hxx\\)\\)$\" > ./cscope.files" dir-name))
	(shell-command "cscope -b"))
	;(eshell-command "cat ./cscope.files | etags -L -"))

(setq tags-table-list '("./"))

;;; conf function 
(defun load-user-config ()
	"load configuration for irony's clang_complete and Makefile"
	(interactive)
	(if (not (file-exists-p "./.clang_complete"))
			(eshell-command "cp ~/.emacs.d/conf/.clang_complete ./"))
	(if (not (file-exists-p "./Makefile"))
			(eshell-command "cp ~/.emacs.d/conf/Makefile ./"))
	(if (not (file-exists-p "./Makefile.root"))
			(eshell-command "cp ~/.emacs.d/conf/MultipleModulesMultipleExecutablesMakefiles/Makefile ./Makefile.root"))
	(if (not (file-exists-p "./Makefile.module"))
			(eshell-command "cp ~/.emacs.d/conf/MultipleModulesMultipleExecutablesMakefiles/A/Makefile ./Makefile.module"))
	(if (not (file-exists-p "./Makefile.exe"))
			(eshell-command "cp ~/.emacs.d/conf/MultipleModulesMultipleExecutablesMakefiles/Exe/Makefile ./Makefile.exe")))

;;; reverts all buffers that are visiting a file.
(defun revert-all-buffers ()
	"Refreshes all open buffers from their respective files."
	(interactive)
	(dolist (buf (buffer-list))
		(with-current-buffer buf
			(when (and (buffer-file-name) (file-exists-p (buffer-file-name)) (not (buffer-modified-p)))
				(revert-buffer t t t) )))
	(message "Refreshed open files.") )


(defun grep-curdir-symbol-at-point ()
  "Grep current directory for symbol at cursor."
  (interactive)
  (grep (concat "unset GREP_OPTIONS && find . -type f ! -path .git ! -name '*.o' ! -name '*.vcxproj' ! -name 'TAGS' ! -name 'cscope.files' ! -name 'cscope.out' ! -name '*#' ! -name '*~'  | xargs -I {} grep --exclude='*.o' -exclude-dir=.git -nH -e '" (current-word)  "' {} | cat -")) )

;;;;;;;;;;;;;;;;;;;;;;;; simple setting ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; not to change dir automatically when opening new files
(add-hook 'find-file-hook
	  (lambda ()
	    (setq default-directory command-line-default-directory)))

;;; do not display toolbar
(tool-bar-mode -1)

;;; indent
(setq-default c-basic-offset 2 c-default-style "linux")
;(define-key c-mode-base-map (kbd "RET") 'newline-and-indent)
(define-key global-map (kbd "RET") 'newline-and-indent)

;C-M-/ and M-/ is bound to complete dabbrev
;;; complete path
(global-set-key "\M-/" 'comint-dynamic-complete-filename)

(global-set-key (kbd "<f6>") 'grep-curdir-symbol-at-point)

;;; line number
(global-linum-mode 1)

;;; "_" as word characters
(modify-syntax-entry ?_ "w")

(setq-default tab-width 2 indent-tabs-mode nil)

(setq inhibit-startup-message t)

;;; c++-mode for header file
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))


;;;;;;;;;;;;;;;;;;;;; not working config, for ref ;;;;;;;;;;;;;;;;;;;;;;
;;; these lines are supposed to make better indent for python
;;; but make elpy unable to work correctly
;(add-hook 'python-mode-hook 'guess-style-guess-tabs-mode)
;(add-hook 'python-mode-hook (lambda ()
;                              (guess-style-guess-tab-width)))


;;;;;;;;;;;;;;;;;;;;; project setting ;;;;;;;;;;;;;;;;;;;;;;;
;;; example: 
;;; (ede-cpp-root-project "Test"
;;;                 :name "Test Project"
;;;                 :file "~/work/project/CMakeLists.txt"
;;;                 :include-path '("/"
;;;                                 "/Common"
;;;                                 "/Interfaces"
;;;                                 "/Libs"
;;;                                )
;;;                 :system-include-path '("~/exp/include")
;;;                 :spp-table '(("isUnix" . "")
;;;;                              ("BOOST_TEST_DYN_LINK" . "")))
;(ede-cpp-root-project "Caffe"
;                :name "Caffe"
;                :file "~/code/caffe/Makefile"
;                :include-path '("~/code/caffe/include" "~/code/caffe/build/src"
;                               )
;                :system-include-path '("/usr/include" "/usr/local/include")
;                :spp-table '(("USE_OPENCV" . "")
;                             ("WITH_PYTHON_LAYER" . "")))

;;;;;;;;;;;;;;;;;;;;; ecb setting ; (working, good) ;;;;;;;;;;;;;;;;;;;;;;;;
(require 'ecb)
;(ecb-activate)
;(ecb-layout-name 'left3)

;;;;;;;;;;;;;;;;;;;;;;;; ecb setting ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ecb-options-version "2.40")
 '(package-selected-packages
   (quote
    (python-environment pos-tip icicles evil epc elpy ein ecb company-irony autopair auto-install auto-complete-exuberant-ctags auto-compile))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
