;;; xcscope-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (cscope-setup cscope-minor-mode) "xcscope" "../../../.emacs.d/elpa/xcscope-20140510.1437/xcscope.el"
;;;;;;  (22133 57723 730927 759000))
;;; Generated autoloads from ../../../.emacs.d/elpa/xcscope-20140510.1437/xcscope.el

(autoload 'cscope-minor-mode "xcscope" "\
This cscope minor mode maps cscope keybindings to make cscope
functions more accessible.

Key bindings:
\\{cscope-minor-mode-keymap}

\(fn &optional ARG)" t nil)

(autoload 'cscope-setup "xcscope" "\
Automatically turns on cscope-minor-mode when editing C and
C++ sources

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("../../../.emacs.d/elpa/xcscope-20140510.1437/xcscope-pkg.el")
;;;;;;  (22133 57723 740529 615000))

;;;***

(provide 'xcscope-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; xcscope-autoloads.el ends here
