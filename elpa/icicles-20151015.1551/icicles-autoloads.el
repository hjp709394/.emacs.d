;;; icicles-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "icicles" "../../../.emacs.d/elpa/icicles-20151015.1551/icicles.el"
;;;;;;  (22133 11108 961761 18000))
;;; Generated autoloads from ../../../.emacs.d/elpa/icicles-20151015.1551/icicles.el
 (autoload 'icy-mode    "icicles" "Toggle Icicle mode - see `icicle-mode'." t nil)
 (autoload 'icicle-mode "icicles" 
"Icicle mode: Toggle minibuffer input completion and cycling.
 Non-nil prefix ARG turns mode on if ARG > 0, else turns it off.
 Icicle mode is a global minor mode.  It binds keys in the minibuffer.
 \ 
 For more information, use `\\<minibuffer-local-completion-map>\\[icicle-minibuffer-help]' \
 when the minibuffer is active.
 \ 
 Depending on your platform, if you use Icicles in a text terminal
 \(that is, without a window system/manager), you might need to change
 some of the key bindings if some of the default bindings are not
 available to you.
 \ 
 Icicle mode defines many top-level commands.  For a list, see the
 Commentary headers of files `icicles-cmd1.el' and `icicles-cmd2.el'."
 t nil)

;;;***

;;;### (autoloads nil nil ("../../../.emacs.d/elpa/icicles-20151015.1551/icicles-chg.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-cmd1.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-cmd2.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-doc1.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-doc2.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-face.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-fn.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-mac.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-mcmd.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-mode.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-opt.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-pkg.el"
;;;;;;  "../../../.emacs.d/elpa/icicles-20151015.1551/icicles-var.el")
;;;;;;  (22133 11109 92717 291000))

;;;***

(provide 'icicles-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; icicles-autoloads.el ends here
